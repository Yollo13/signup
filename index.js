let togglePassword = document.getElementById('togglePassword');
let toggleConfirmPassword = document.getElementById('toggleConfirmPassword')
let password = document.getElementById('passwordInput');
let confirmPassword = document.getElementById('confirmPasswordInput')

togglePassword.addEventListener('click', function () {
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    
    if(type === 'password') {
        togglePassword.src = 'resources/icon-general-hide.svg'
    } else {
        togglePassword.src = 'resources/icon-general-show.svg'
    }
});

toggleConfirmPassword.addEventListener('click', function () {
    const type = confirmPassword.getAttribute('type') === 'password' ? 'text' : 'password';
    confirmPassword.setAttribute('type', type);
    
    if(type === 'password') {
        toggleConfirmPassword.src = 'resources/icon-general-hide.svg'
    } else {
        toggleConfirmPassword.src = 'resources/icon-general-show.svg'
    }
});

let signUpButton = document.getElementById('submitButton')

flatpickr('#birthDate', {
    dateFormat: "d-m-Y",
    monthSelectorType: 'static'
})


//--Inputs

let lastName = document.getElementById('lastNameInput')
let firstName = document.getElementById('firstNameInput')
let email = document.getElementById('emailInput')
let choose = document.getElementById('choiseSelect')
let licenseNumber = document.getElementById('licenseNumberInput')
let phoneNumber = document.getElementById('phoneNumberInput')
let checkBoxFirst = document.getElementById('checkBoxFirst')
let checkBoxSecond = document.getElementById('checkBoxSecond')
let licenseNumberLabel = document.getElementById('licenseNumberLabel')

//--ErrorMessages

let lastNameError = document.getElementById('lastNameErrorMessage')
let firstNameError = document.getElementById('firstNameErrorMessage')
let phoneError = document.getElementById('phoneErrorMessage')
let passwordError = document.getElementById('passwordErrorMessage')
let passwordLengthError = document.getElementById('passwordLengthMessage')
let emailError = document.getElementById('emailErrorMessage')
let licenseNumberError = document.getElementById('licenseNumberErrorMessage')


let regexNames = /^[a-z ,.'-]+$/i
let regexNumbers = /^[0-9]*$/g
let regexEmail = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g

let selectedOption = choose.options[choose.selectedIndex].value

// let inputs = document.querySelectorAll('input')
// console.log(inputs)

// for (let i = 0; i < inputs.length; i++) {
//     if(inputs[i].type === 'text' && inputs[i].hasAttribute('class', 'text')) {
//         if (inputs[i].value !== '' && inputs[i].value.match(regexNames) === null) {
//             nameError[i].style.display = 'block'
//             inputs[i].setAttribute('class', 'error--lastName--Input')
//             signUpButton.disabled = true;
//         } else {
//             nameError[i].style.display = 'none'
//             inputs[i].removeAttribute('class', 'error--lastName--Input')
//             signUpButton.removeAttribute('disabled')
//         }
//     }
// }


function onTyping() {

    if (lastName.value !== '' && lastName.value.match(regexNames) === null) {
        lastNameError.style.display = 'block'
        lastName.setAttribute('class', 'error--lastName--Input')
        signUpButton.disabled = true;
    } else {
        lastNameError.style.display = 'none'
        lastName.removeAttribute('class', 'error--lastName--Input')
        signUpButton.removeAttribute('disabled')
    }

    if (firstName.value !== '' && firstName.value.match(regexNames) === null) {
        firstNameError.style.display = 'block'
        firstName.setAttribute('class', 'error--firstName--Input')
        signUpButton.disabled = true;
    } else {
        firstNameError.style.display = 'none'
        firstName.removeAttribute('class', 'error--firstName--Input')
        signUpButton.removeAttribute('disabled')
    }
    
    if (email.value !== '' && email.value.match(regexEmail) === null) {
        emailError.style.display = 'block'
        email.setAttribute('class', 'error--Email--Input')
        signUpButton.disabled = true;
    } else {
        emailError.style.display = 'none'
        email.removeAttribute('class', 'error--Email--Input')
        signUpButton.removeAttribute('disabled')
    }

    if (licenseNumber.value !== '' && licenseNumber.value.length < 10 || licenseNumber.value.length > 10 || licenseNumber.value.match(regexNumbers) === null) {
        licenseNumberError.style.display = 'block'
        licenseNumberLabel.style.display = 'none'
        licenseNumber.setAttribute('class', 'error--LicenseNumber--Input')
        signUpButton.disabled = true
    } else {
        licenseNumberError.style.display = 'none'
        licenseNumberLabel.style.display = 'block'
        licenseNumber.removeAttribute('class', 'error--LicenseNumber--Input')
        signUpButton.removeAttribute('disabled')
    }
    
    if (phoneNumber.value !== '' && phoneNumber.value.match(regexNumbers) === null) {
        phoneError.style.display = 'block'
        phoneNumber.setAttribute('class', 'error--Phone--Input')
        signUpButton.disabled = true;
    } else {
        phoneError.style.display = 'none'
        phoneNumber.removeAttribute('class', 'error--Phone--Input')
        signUpButton.removeAttribute('disabled')
    }

    
    if (confirmPassword.value !== '' && password.value !== '' && confirmPassword.value !== password.value) {
        passwordError.style.display = 'block'
        password.setAttribute('class', 'error--Password--Input')
        confirmPassword.setAttribute('class', 'error--Password--Input')
        signUpButton.disabled = true;
    } else {
        passwordError.style.display = 'none'
        password.removeAttribute('class', 'error--Password--Input')
        confirmPassword.removeAttribute('class', 'error--Password--Input')
        signUpButton.removeAttribute('disabled')
    }
    
    if (confirmPassword.value !== '' && confirmPassword.value === password.value && password.value.length < 8) {
        passwordLengthError.style.display = 'block'
        passwordError.style.display = 'none'
        signUpButton.disabled = true;
    } else {
        passwordLengthError.style.display = 'none'
    }
    
    if (lastName.value !== '' && 
        firstName.value !== '' &&
        email.value !== '' &&
        selectedOption.value !== '' &&
        licenseNumber.value !== '' && 
        phoneNumber.value !== '' &&
        checkBoxFirst.checked && 
        checkBoxSecond.checked && 
        email.hasAttribute('class', 'error--Email--Input') === false && 
        phoneNumber.hasAttribute('class', 'error--Phone--Input') === false && 
        password.hasAttribute('class', 'error--Password--Input') === false &&
        confirmPassword.hasAttribute('class', 'error--Password--Input') === false &&
        password.hasAttribute('class', 'error--Length') === false &&
        licenseNumber.hasAttribute('class', 'error--LicenseNumber--Input') === false) 
    {
        signUpButton.removeAttribute('disabled')
    } else {
        signUpButton.removeAttribute('disabled')
        signUpButton.disabled = true;
    }
}


var doc, bod, htm;
addEventListener('load', function(){
    doc = document; bod = doc.body; htm = doc.documentElement;
    addEventListener('scroll', function(){
        doc.getElementById('registrationButtonContainer').setAttribute('class', htm.scrollTop > 50 ? 'bottom' : 'top')
    });
});



function signUp(event) {
    event.preventDefault()
    document.getElementById('signUpContainer').style.display = 'none'
    document.getElementById('successfullRegistrationContainer').style.display = 'flex'
    let circle1 = document.getElementById('firstCircle')
    let circle2 = document.getElementById('secondCircle')
    let circle3 = document.getElementById('thirdCircle')
    let circle4 = document.getElementById('fourthCircle')
    let bigCircle = document.getElementById('bigCircle')
    let animatedCircle = new TimelineMax();
    animatedCircle.fromTo(circle1, 0.7, {y: "20", x: "20"}, {y: "-20", x: "-20"})
    animatedCircle.fromTo(circle2, 0.7, {y: "40", x: "-40"}, {y: "-10", x: "10"}, 0)
    animatedCircle.fromTo(circle3, 0.7, {y: "80", x: "60"}, {y: "0", x: "0"}, 0)
    animatedCircle.fromTo(circle4, 0.7, {y: "0", x: "0"}, {y: "40", x: "-30"}, 0)
    animatedCircle.fromTo(bigCircle, 0.5, {y: "80"}, {y: "0"}, 0)
}